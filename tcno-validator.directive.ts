import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, FormControl } from '@angular/forms';

function validateTcnoFactory() {
  return (c: FormControl) => {
    let a = c.value ? c.value.toString() : '';
    let result = true;

    if(a.length < 1) {
      return null;
    }
    if(a.substr(0,1)==0 || a.length!=11){
      result = false;
    }

    if(result && !/(^\d+$)/.test(a)) {
       result = false;
    }

    let i = 9, md='', mc='', digit, mr='';

    while(digit = a.charAt(--i)){
      i%2==0 ? md += digit : mc += digit;
    }
    if(result && ((eval(md.split('').join('+'))*7)-eval(mc.split('').join('+')))%10!=parseInt(a.substr(9,1),10)){
      result = false;
    }
    for (let c=0;c<=9;c++){
      mr += a.charAt(c);
    }
    if(result && eval(mr.split('').join('+'))%10!=parseInt(a.substr(10,1),10)){
      result = false;
    }

    return result ? null : {
      validateTc: {
        valid: false
      }
    }
  };
}

@Directive({
  selector: '[validateTc][ngModel],[validateTc][formControl]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => TcnoValidator), multi: true }
  ]
})
export class TcnoValidator {

  validator: Function;

  constructor() {
    this.validator = validateTcnoFactory();
  }

  validate(c: FormControl) {
    return this.validator(c);
  }
}
