// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { IndicatorComponent } from './indicator.component';

@NgModule({
    imports: [

    ],
    declarations: [
        IndicatorComponent,
    ],
    exports: [
        IndicatorComponent,
    ]
})
export class IndicatorModule {

}
