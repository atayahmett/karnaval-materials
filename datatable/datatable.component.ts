import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'datatable',
    templateUrl: './datatable.component.html',
})
export class DatatableComponent {
   @Input() data: Array<any>;
   @Input() options: any = {};
   @Output() deleteEventEmitter = new EventEmitter();

  constructor() {
    // alert(this.data);
  }

  public doSort(option: any)
  {
    if('sort' in option) {
      option['sort'] = option.sort == 'asc' ? 'desc': 'asc';
    }else{
      option['sort'] = 'desc';
    }
  }

  public doDelete(item: any)
  {
    this.deleteEventEmitter.emit(item);
  }
}
