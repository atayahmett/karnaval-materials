// Angular Imports
import { NgModule } from '@angular/core';
import {CommonModule} from "@angular/common";
import {Routes, RouterModule} from '@angular/router';

// This Module's Components
import { DatatableComponent } from './datatable.component';

@NgModule({
    imports: [
      CommonModule,
      RouterModule
    ],
    declarations: [
        DatatableComponent,
    ],
    exports: [
        DatatableComponent,
        RouterModule
    ]
})
export class DatatableModule {

}
