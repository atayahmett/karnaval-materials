import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'pagination',
    templateUrl: 'pagination.component.html',
})
export class PaginationComponent {
  @Input() public total: number;
  @Input() public per_page: number;
  @Input() public current_page: number;
  @Input() public last_page: number;
  @Input() public segments: string = '/panel/ticketlar';

  constructor(private router: Router, private activatedRoute: ActivatedRoute)
  {
    //console.log(this.activatedRoute.snapshot.queryParams['page']);
  }

  ngOnInit()
  {

  }

  public page(num: number): void
  {
    var params = {};
    const currentParams = this.activatedRoute.snapshot.queryParams;

    if(Object.isExtensible(params)) {

      for(let key in currentParams) params[key] = currentParams[key];

      params['page'] = num;
    }else{
      params = currentParams;
    }

    this.router.navigate([this.segments], { queryParams: params });
  }

  public getPagesNumber(): Array<number>
  {
    return Array(this.last_page).fill(1).map((val, i) => i+1);
  }

  public getPadding(current_page: number, last_page: number, dir: string): number
  {
    const avg = (last_page - current_page);
    let padding: number = 0;

    if(dir == 'left') {
      if(avg <= 2) {
        padding =  (current_page-2) - (2 - avg);
      }else {
        padding = (current_page-2);
      }
    }
    else if(dir == 'right') {
      if(last_page <= (avg+2)) {
        padding = (current_page+2) + (3 - (last_page - avg));
      }else{
        padding = (current_page+2);
      }
    }
    return padding;
  }
}
