import { Component } from '@angular/core';

import { AlertService } from '../../_services/alert.service';

@Component({
  selector: 'generic-messages',
  template: `
    <div *ngIf="alerts && getList(alerts)">
      <alert type="{{alerts.type}}">
        <div *ngFor="let message of getList(alerts)">{{message}}</div>
      </alert>
    </div>
  `
})
export class GenericMessagesComponent {
  public alerts: any;
  public keyBind: any = {
    'danger' : 'error',
    'success': 'success',
    'info'   : 'info',
    'warning': 'warning'
  };

  constructor(private alertService: AlertService) {

    console.log('Alert component');
  }

  ngOnInit() {
      this.alertService.getMessage().subscribe(messages => { this.alerts = messages; });
  }

  getList(alerts)
  {
    const key = this.keyBind[alerts.type];

    return alerts.text[key];
  }
}
