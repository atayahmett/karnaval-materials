/**
 * Created by orcun @ BayiDostu on 30/01/17.
 */

import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {environment} from "environments/environment";

@Injectable()
export class AuthGuestGuard implements CanActivate {
    
    private route: string = '/site/anasayfa';

    constructor(private _router: Router) {}
    
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean
    {
        if(! localStorage.getItem('currentUser'))
        {
            // user logged in, so it returns true
            return true;
        }
        
        // if user logged in, so redirect the home page
        this._router.navigate([this.route]);
        
        return false;
    }
}