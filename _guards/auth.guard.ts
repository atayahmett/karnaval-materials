/**
 * Created by orcun @ BayiDostu on 30/01/17.
 */

import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";

@Injectable()
export class AuthGuard implements CanActivate {
    
    private route: string = '/site/giris-yap';

    constructor(private _router: Router) {}
    
    //noinspection TypeScriptUnresolvedVariable
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean
    {
        if(localStorage.getItem('currentUser'))
        {
            // user logged in, so it returns true
            return true;
        }
        
        // user not logged in, so redirect the login page with return url
        this._router.navigate([this.route], {
            queryParams : {
                returnUrl : state.url
            }
        });
        
        return false;
    }
    
    public isLoggedIn() {
        return localStorage.getItem('currentUser') ? true : false;
    }
}